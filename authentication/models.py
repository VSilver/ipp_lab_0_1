from django.db import models
from django.utils import timezone


class App(models.Model):
    id = models.CharField(max_length=10, primary_key=True)

    def __str__(self):
        return self.id


class User(models.Model):
    email = models.CharField(max_length=100, primary_key=True)
    name_surname = models.CharField(max_length=50)
    password = models.CharField(max_length=256)
    salt = models.CharField(max_length=10)

    def __str__(self):
        return self.name_surname


class Session(models.Model):
    token = models.CharField(max_length=20)
    user = models.ForeignKey(User, related_name='sessions')
    time = models.DateTimeField(editable=True, auto_now=True)
    app = models.ForeignKey(App, related_name='sessions')

    class Meta:
        unique_together = ('user', 'app')

    def __str__(self):
        return self.user.email + ' ' + self.token
