import string
import datetime
import random as rand
import hashlib

from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone

from authentication.models import *


def home_page(request):
    return HttpResponse('Hello')


def login(request):
    context_dict = {}
    if request.method == 'POST':
        data = dict(request.POST)
        print(data)
        app_id = data['app_id'][0]
        email = data['email'][0]
        password = data['password'][0]
        if check_app_id_existence(app_id):
            if check_user_existence(email):
                user = find_user(email, password)
                if not user:
                    context_dict['code'] = 3
                    return JsonResponse(context_dict)
                else:
                    session = find_session(user=user, app_id=app_id)
                    if session:
                        if (timezone.now() - session.time).seconds > 300:
                            context_dict['code'] = 0
                            context_dict['token'] = generate_token(20)
                            update_session(session=session, token=context_dict['token'], app_id=app_id, user=user)
                        else:
                            context_dict['code'] = 1
                            return JsonResponse(context_dict)
                    else:
                        context_dict['code'] = 0
                        context_dict['token'] = generate_token(20)
                        session = create_session(user=user, token=context_dict['token'], app_id=app_id)
            else:
                context_dict['code'] = 3
        else:
            context_dict['code'] = 2
        return JsonResponse(context_dict)
    elif request.method == 'GET':
        # print('####################### Items of GET request: ', request.GET.items())
        return render(request, 'login.html')
    else:
        return render(request, 'login.html')


def register(request):
    context_dict = {}
    if request.method == 'POST':
        data = dict(request.POST)
        # print('########## Data: ', data)
        app_id = data['app_id'][0]
        email = data['email'][0]
        name_surname = data['name_surname'][0]
        password = data['password'][0]
        if check_app_id_existence(app_id):
            if not check_user_existence(email):
                token = generate_token(20)
                context_dict['code'] = 0
                context_dict['token'] = token

                sha256 = hashlib.sha256()
                salt = generate_token(10)
                password += salt
                password = password.encode()
                sha256.update(password)
                hashed_password = sha256.hexdigest()

                user = save_new_user(email, name_surname, hashed_password, salt)
                create_session(user=user, token=token, app_id=app_id)
            else:
                context_dict['code'] = 1
        else:
            # print("############## Couldn't find the app ", app_id)
            context_dict['code'] = 2
        return JsonResponse(context_dict)
    else:
        return render(request, 'register.html')


def get_last_login(request):
    context_dict = {}
    if request.method == 'POST':
        data = dict(request.POST)
        app_id = data['app_id'][0]
        email = data['email'][0]
        token = data['token'][0]
        if check_app_id_existence(app_id):
            if check_user_existence(email):
                user = find_user_by_email(email)
                app = find_app(app_id)
                if check_token_matching(token, user, app):
                    session = find_session_by_user_token(user, token)
                    context_dict['date_time'] = get_date_time_login(session)
                    context_dict['code'] = 0
                else:
                    context_dict['code'] = 4
            else:
                context_dict['code'] = 3
        else:
            context_dict['code'] = 2
        return JsonResponse(context_dict)
    return HttpResponse('Get last login time')


def get_my_logged_in_apps(request):
    context_dict = {}
    if request.method == 'POST':
        data = dict(request.POST)
        app_id = data['app_id'][0]
        email = data['email'][0]
        token = data['token'][0]
        if check_app_id_existence(app_id):
            if check_user_existence(email):
                user = find_user_by_email(email)
                app = find_app(app_id)
                if check_token_matching(token, user, app):
                    apps = find_logged_in_apps_by_user(user)
                    context_dict['app_ids'] = apps
                    context_dict['code'] = 0
                else:
                    context_dict['code'] = 4
            else:
                context_dict['code'] = 3
        else:
            context_dict['code'] = 2
        return JsonResponse(context_dict)
    context_dict['code'] = 3
    return JsonResponse(context_dict)


def logout(request):
    context_dict = {}
    if request.method == 'POST':
        data = dict(request.POST)
        app_id = data['app_id'][0]
        email = data['email'][0]
        token = data['token'][0]
        if check_app_id_existence(app_id):
            if check_user_existence(email):
                user = find_user_by_email(email)
                app = find_app(app_id)
                if check_token_matching(token, user, app):
                    session = find_session(user, app_id)
                    session.delete()
                    context_dict['code'] = 0
                else:
                    context_dict['code'] = 4
            else:
                context_dict['code'] = 3
        else:
            context_dict['code'] = 2
        return JsonResponse(context_dict)
    context_dict['code'] = 3
    return JsonResponse(context_dict)


# Custom functions

def create_session(user, token, app_id):
    session = Session(user=user, token=token, app_id=app_id, time=datetime.date.today())
    session.save()
    return session


def find_session(user, app_id):
    try:
        session = Session.objects.get(user=user, app_id=app_id)
        return session
    except ObjectDoesNotExist:
        return None


def find_session_by_user_token(user, token):
    try:
        session = Session.objects.get(user=user, token=token)
        return session
    except ObjectDoesNotExist:
        return None


def update_session(session, token, app_id, user):
    session.token = token
    session.user = user
    session.app_id = app_id
    session.save()


def save_new_user(email, name_surname, hashed_password, salt):
    user = User(email=email, name_surname=name_surname, password=hashed_password, salt=salt)
    user.save()
    return user


def find_user(email, password):
    try:
        salt = User.objects.get(email=email).salt
        sha256 = hashlib.sha256()
        sha256.update((password + salt).encode())
        hashed_password = sha256.hexdigest()
        user = User.objects.get(email=email, password=hashed_password)
        return user
    except ObjectDoesNotExist:
        return None


def find_user_by_email(email):
    try:
        user = User.objects.get(email=email)
        return user
    except ObjectDoesNotExist:
        return None


def find_app(app_id):
    try:
        app = App.objects.get(id=app_id)
        return app
    except ObjectDoesNotExist:
        return None


def check_app_id_existence(app_id):
    return App.objects.filter(pk=app_id).exists()


def check_user_existence(email):
    return User.objects.filter(email=email).exists()


def generate_token(length):
    alphabet = string.ascii_letters + string.digits
    random = rand.SystemRandom()
    token = ''.join(random.choice(alphabet) for _ in range(length))
    return token


def check_token_matching(token, user, app):
    return Session.objects.filter(token=token, user=user, app=app).exists()


def get_date_time_login(session):
    return session.time.strftime("%B %d, %Y %H:%M:%S")


def find_logged_in_apps_by_user(user):
    return [session.app.id for session in Session.objects.filter(user=user)]
