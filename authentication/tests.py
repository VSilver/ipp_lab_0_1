from django.test import TestCase
from django.core.urlresolvers import resolve
from django.http import HttpResponse, HttpRequest, JsonResponse
from django.core.exceptions import ObjectDoesNotExist

import json
import hashlib
from datetime import datetime

from authentication import views
from authentication.models import *


class UserModelTest(TestCase):

    def test_saving_user(self):
        user = User()
        user.email = 'email1'
        user.password = 'password1'
        user.name_surname = 'name_surname1'
        user.save()

        self.assertEqual(User.objects.count(), 1)

    def test_retrieving_user(self):
        user = User()
        user.email = 'email1'
        user.password = 'password1'
        user.name_surname = 'name_surname1'
        user.save()

        self.assertEqual(User.objects.first(), user)


class AppModelTest(TestCase):

    def test_saving_app(self):
        app = App()
        app.id = 'app_id1'
        app.save()

        self.assertEqual(App.objects.count(), 1)

    def test_retrieving_app(self):
        app = App()
        app.id = 'app_id1'
        app.save()

        self.assertEqual(App.objects.first(), app)


class LoginTest(TestCase):

    def test_login_url_resolves_to_login_view(self):
        found = resolve('/login/')
        self.assertEqual(found.func, views.login)

    def test_response_code_of_wrong_request(self):
        request = self.return_login_http_request(None, '', '')
        response = json.loads(views.login(request).content.decode())
        self.assertNotEqual(response['code'], 0)

    def test_successful_login(self):
        request = self.return_login_http_request('email1', 'password1', 'app_id1')
        response = json.loads(views.login(request).content.decode())
        self.assertEqual(response['code'], 0)

    def test_json_response_after_POST_request(self):
        request = self.return_login_http_request('email1', 'password1', 'app_id1')
        response = views.login(request)
        self.assertEqual(response.__class__, JsonResponse)

    def test_http_response_after_simple_request(self):
        request = HttpRequest()
        response = views.login(request)
        self.assertTrue(isinstance(response, HttpResponse))

    def test_home_page_can_save_a_POST_request(self):
        request = self.return_login_http_request('email1', 'password1', 'app_id1')
        views.login(request)
        session = Session.objects.first()
        self.assertEqual(session.user.email, 'email1')
        self.assertEqual(session.app_id, 'app_id1')

    def test_impossibility_to_login_the_same_user_twice_from_same_app_without_logout(self):
        request = self.return_login_http_request('email1', 'password1', 'app_id3')
        response1 = views.login(request)
        response2 = views.login(request)
        response_dict1 = json.loads(response1.content.decode())
        response_dict2 = json.loads(response2.content.decode())
        self.assertEqual(response_dict1['code'], 0)
        self.assertNotEqual(response_dict1['code'], response_dict2['code'])

    def test_multiple_user_login_from_different_apps(self):
        request = self.return_login_http_request('email1', 'password1', 'app_id3')
        response1 = json.loads(views.login(request).content.decode())
        request = self.return_login_http_request('email1', 'password1', 'app_id2')
        response2 = json.loads(views.login(request).content.decode())
        self.assertEqual(response1['code'], 0)
        self.assertEqual(response2['code'], 0)
        self.assertNotEqual(response1['token'], response2['token'])

    def return_login_http_request(self, email, password, app_id):
        app = App()
        app.id = app_id
        app.save()

        request = HttpRequest()
        request.method = 'POST'
        request.POST['email'] = email
        request.POST['password'] = password
        request.POST['app_id'] = app_id
        request.POST['name_surname'] = 'name1'

        views.register(request)

        for s in Session.objects.all():
            s.delete()

        return request


class RegisterTest(TestCase):

    def test_register_url_resolves_to_register_view(self):
        found = resolve('/register/')
        self.assertEqual(found.func, views.register)

    def test_same_user_data_multiple_registration(self):
        request = self.return_register_http_request('email1', 'password1', 'app_id1', 'name_surname1')
        response1 = json.loads(views.register(request).content.decode())
        response2 = json.loads(views.register(request).content.decode())
        self.assertEqual(response1['code'], 0)
        self.assertNotEqual(response2['code'], 0)

    def test_user_password_is_salted_and_hashed(self):
        request1 = self.return_register_http_request('email1', 'password1', 'app_id1', 'name_surname1')
        views.register(request1)
        request2 = self.return_register_http_request('email2', 'password1', 'app_id2', 'name_surname2')
        views.register(request2)
        user1 = User.objects.get(email='email1')
        user2 = User.objects.get(email='email2')

        salt = user1.salt
        sha256 = hashlib.sha256()
        sha256.update(('password1' + salt).encode())
        password = sha256.hexdigest()
        self.assertEqual(user1.password, password)

        salt = user2.salt
        sha256 = hashlib.sha256()
        sha256.update(('password1' + salt).encode())
        password = sha256.hexdigest()
        self.assertEqual(user2.password, password)

        self.assertNotEqual(request1.POST['password'], user1.password)
        self.assertNotEqual(request2.POST['password'], user2.password)
        self.assertNotEqual(user1.password, user2.password)

    def return_register_http_request(self, email, password, app_id, name_surname):
        app = App()
        app.id = app_id
        app.save()

        request = HttpRequest()
        request.method = 'POST'
        request.POST['app_id'] = app_id
        request.POST['email'] = email
        request.POST['name_surname'] = name_surname
        request.POST['password'] = password
        return request


class GetLastLoginTest(TestCase):

    def test_get_last_login_url_resolves_to_login_view(self):
        found = resolve('/get_last_login/')
        self.assertEqual(found.func, views.get_last_login)

    def test_get_last_login(self):
        request = self.return_get_last_login_http_request('email1', 'token1', 'app_id1')
        response = json.loads(views.get_last_login(request).content.decode())
        self.assertEqual(response['code'], 0)
        time1 = timezone.now()
        time2 = timezone.make_aware(
            datetime.strptime(response['date_time'], '%B %d, %Y %H:%M:%S'),
            timezone.get_current_timezone()
        )
        self.assertLessEqual((time1 - time2).seconds, 0)

    def test_response_code_of_wrong_request(self):
        request = HttpRequest()
        request.method = 'GET'
        request.GET['id'] = 3

        response = views.get_last_login(request)
        self.assertEqual(response.__class__, HttpResponse)
        if not isinstance(response, HttpResponse):
            response = json.loads(response.content)
            self.assertNotEqual(response['code'], 0)

    def return_get_last_login_http_request(self, email, token, app_id):
        app = App()
        app.id = app_id
        app.save()

        user = User()
        user.email = email
        user.name_surname = 'name1'
        user.password = 'password1'
        user.save()

        session = Session()
        session.user = user
        session.app = app
        session.time = timezone.now()
        session.token = token
        session.save()

        request = HttpRequest()
        request.method = 'POST'
        request.POST['email'] = email
        request.POST['token'] = token
        request.POST['app_id'] = app_id
        return request


class GetMyLoggedInAppsTest(TestCase):

    def test_get_my_logged_in_aps_url_resolves_to_login_view(self):
        found = resolve('/get_my_logged_in_apps/')
        self.assertEqual(found.func, views.get_my_logged_in_apps)

    def test_response_code_of_wrong_request(self):
        request = HttpRequest()
        request.method = 'GET'

        response = json.loads(views.get_my_logged_in_apps(request).content.decode())

        self.assertNotEqual(response['code'], 0)

    def test_get_user_logged_in_apps_at_the_moment(self):
        app = App()
        app.id = 'app_id1'
        app.save()
        app.id = 'app_id2'
        app.save()

        request = RegisterTest().return_register_http_request(
            'email1', 'password1', 'app_id1', 'name_surname'
        )
        views.register(request)

        request = LoginTest().return_login_http_request('email1', 'password1', 'app_id2')
        views.login(request)

        request = LoginTest().return_login_http_request('email1', 'password1', 'app_id1')
        response_login = json.loads(views.login(request).content.decode())

        request = HttpRequest()
        request.method = 'POST'
        request.POST['email'] = 'email1'
        request.POST['token'] = response_login['token']
        request.POST['app_id'] = 'app_id1'

        response_logged_in_apps = json.loads(views.get_my_logged_in_apps(request).content.decode())

        # select app.name
        # from session natural join app
        # where session.email = 'email1'

        sessions = Session.objects.filter(user__email='email1')
        app_ids = [s.app.id for s in sessions]

        self.assertEqual(app_ids, response_logged_in_apps['app_ids'])


class LogOutViewTest(TestCase):

    def test_log_out(self):
        app = App()
        app.id = 'app_id1'
        app.save()

        request = RegisterTest().return_register_http_request(
            'email1', 'password1', 'app_id1', 'name_surname'
        )
        views.register(request)

        request = LoginTest().return_login_http_request('email1', 'password1', 'app_id1')
        response = json.loads(views.login(request).content.decode())

        token = response['token']

        request = HttpRequest()
        request.method = 'POST'
        request.POST['token'] = token
        request.POST['app_id'] = 'app_id1'
        request.POST['email'] = 'email1'

        response = json.loads(views.logout(request).content.decode())

        self.assertEqual(response['code'], 0)
        self.assertRaises(ObjectDoesNotExist, lambda: Session.objects.get(token=token, app=app, user__email='email1'))
