from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.home_page, name='home'),
    url(r'^register/', views.register, name='register'),
    url(r'^login/', views.login, name='login'),
    url(r'^get_last_login/', views.get_last_login, name='get_last_login'),
    url(r'^get_my_logged_in_apps/', views.get_my_logged_in_apps, name='get_my_logged_in_apps'),
    url(r'^logout/', views.logout, name='logout'),
]