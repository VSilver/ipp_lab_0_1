# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='App',
            fields=[
                ('id', models.CharField(primary_key=True, serialize=False, max_length=10)),
            ],
        ),
        migrations.CreateModel(
            name='Session',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
                ('token', models.CharField(max_length=20)),
                ('time', models.DateTimeField()),
                ('app', models.ForeignKey(to='authentication.App', related_name='sessions')),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('email', models.CharField(primary_key=True, serialize=False, max_length=100)),
                ('name_surname', models.CharField(max_length=50)),
                ('password', models.CharField(max_length=256)),
                ('salt', models.CharField(max_length=10)),
            ],
        ),
        migrations.AddField(
            model_name='session',
            name='user',
            field=models.ForeignKey(to='authentication.User', related_name='sessions'),
        ),
        migrations.AlterUniqueTogether(
            name='session',
            unique_together=set([('user', 'app')]),
        ),
    ]
